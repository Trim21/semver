#!/usr/bin/env node

const simpleGit = require('simple-git/promise')(process.cwd())
const semver = require('semver')

const action = process.argv[2]

async function main () {
  let tags = (await simpleGit.tags()).all
  let t = tags.filter((v: string) => {
    return semver.valid(v)
  })
  t.sort(semver.compare)
  let newTag: string | undefined = undefined
  if (action === 'alpha' || action === 'beta') {
    newTag = semver.inc(t[t.length - 1], 'prerelease', action)
  } else if (['patch', 'minor', 'major'].includes(action)) {
    newTag = semver.inc(t[t.length - 1], action)
  }
  if (newTag !== undefined) {
    await simpleGit.raw(['commit', '--allow-empty', '-m', `v${newTag}`])
    await simpleGit.addTag(`v${newTag}`)
    console.log(newTag)
  }
}

main().catch(e => {
  console.error(e)
  process.exit(1)
})
